from flask import Flask
from redis import Redis

from celery_manager import make_celery


app = Flask(__name__)
app.config.from_object('config')

celeryApp = make_celery(app)
redis_client = Redis(host=app.config['REDIS_HOST'], port=app.config['REDIS_PORT'], decode_responses=True)


@app.route('/')
def view():
    data = redis_client.hgetall('weather')

    return '<br \\>'.join(k + '\t' + v for k, v in data.items())


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
