from celery import Celery

import celeryconfig


def make_celery(app):
    celeryApp = Celery(
        app.import_name,
        broker=app.config['CELERY_BROKER_URL']
    )

    celeryApp.conf.update(app.config)
    celeryApp.config_from_object(celeryconfig)

    TaskBase = celeryApp.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celeryApp.Task = ContextTask

    return celeryApp
