import requests
from datetime import datetime

from app import celeryApp, redis_client

CONNECTION_TIMEOUT = 5
READ_TIMEOUT = 30

YEREVAN_COORDS = ('40.1872', '44.5152')
API_KEY = 'd0ebf47dd3cc4ad3a25dfa7350e902ad'
UNIT = 'si'
API_URL = 'https://api.darksky.net/forecast/{api_key}/{lat},{lng}'.format(api_key=API_KEY,
                                                                          lat=YEREVAN_COORDS[0],
                                                                          lng=YEREVAN_COORDS[1])
QUERY_PARAMS = {'units': UNIT}


@celeryApp.task()
def fetch_weather():
    response = requests.get(API_URL, params=QUERY_PARAMS, timeout=(CONNECTION_TIMEOUT, READ_TIMEOUT))

    try:
        response = response.json()
    except ValueError:
        pass
    else:
        try:
            temp = response['currently']['temperature']
        except KeyError:
            pass
        else:
            redis_client.hset('weather', str(datetime.now()), temp)
