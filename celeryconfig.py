from os import environ

from celery.schedules import crontab


CELERY_IMPORTS = 'tasks.weather'
CELERY_TASK_RESULT_EXPIRES = environ.get('CELERY_TASK_RESULT_EXPIRES', 30)
CELERY_TIMEZONE = environ.get('CELERY_TIMEZONE', 'UTC')

CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERYBEAT_SCHEDULE = {
    'fetch_weather': {
        'task': 'tasks.weather.fetch_weather',
        # Every minute
        'schedule': crontab(minute="*"),
    }
}
