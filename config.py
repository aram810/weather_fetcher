from os import environ

REDIS_HOST = environ.get('REDIS_HOST', '0.0.0.0')
REDIS_PORT = environ.get('REDIS_PORT', 6379)
CELERY_BROKER_URL = environ.get('CELERY_BROKER_URL', "redis://{host}:{port}/0".format(
    host=REDIS_HOST, port=str(REDIS_PORT)))
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
